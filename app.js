const { format } = require('date-fns')
const express = require('express')
const bluebird = require('bluebird')
const redis = require('redis')

bluebird.promisifyAll(redis)

const client = redis.createClient({
  host: '10.10.20.72',
  port: 6379
})

const port = 3000    
const app = express() 


app.get('/hello', async (req, res) => {
  const limit = 10      
  const path = '/hello' 
  const time = format(new Date(), 'yyyyMMdd:HHmm')
  
  const key = `${req.ip}:${path}:${time}` 

  
  const count = parseInt(await client.getAsync(key))
  if (count > limit) {
    return res.status(429).json({
      error: 429,
      message: `Too Many Request`
    })
  }

  const trx = client.multi()
  trx.incr(key)         
  trx.expire(key, 60)   
  await trx.execAsync()

  res.json({
    message: 'Hello Indri'
  })
})

app.listen(port, () => {
  console.log(`App listening on port ${port}!`)
})